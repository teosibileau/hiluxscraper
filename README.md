# Hilux Scraper

I was hunting a pre 2005 Hilux sr5. In Argentina, they are in very high demand and sometimes very expensive.

I wanted to know:

+ If a new one was published
+ If price was reduced for an existing publication

So I made this scraper. And eventually, it worked, look at this beauty:

![Hilux](.static/hilux.jpg)

I want you to have one. Use this code wisely.

## Requirements

+ [pipx](https://github.com/pypa/pipx)
+ [poetry](https://python-poetry.org/docs/#installing-with-pipx)
+ [ahoy](https://github.com/ahoy-cli/ahoy)

## Installation

```bash
ahoy install
```

## Usage

```bash
ahoy scrap
```

Data is saved to `data/db.json`. You can inspect it with `jq`:

```bash
cat data/db.json | jq
```

```json
{
  "_default": {
      "20": {
      "title": "Toyota Hilux 2001 2.8 D/cab 4x4 D Sr5",
      "price": "11.000",
      "currency": "U$S",
      "link": "https://auto.mercadolibre.com.ar/MLA-1540470186-toyota-hilux-2001-28-dcab-4x4-d-sr5-_JM",
      "km": "230.000",
      "year": "2001",
      "meta": "· Publicado hace 33 días",
      "location": "Capital Federal - Capital Federal"
    },
    "21": {
      "title": "Toyota Hilux 2.8 D/cab 4x4 D Sr5",
      "price": "12.500",
      "currency": "U$S",
      "link": "https://auto.mercadolibre.com.ar/MLA-1393759149-toyota-hilux-1998-28-dcab-4x4-d-sr5-_JM",
      "km": "429.180",
      "year": "1998",
      "meta": "· Publicado hace 24 días",
      "location": "Pilar - Bs.As. G.B.A. Norte"
    },
    "22": {
      "title": "Toyota Hilux 2.8 D/cab 4x4 D Sr5",
      "price": "14.500",
      "currency": "U$S",
      "link": "https://auto.mercadolibre.com.ar/MLA-1395446135-toyota-hilux-1999-28-dcab-4x4-d-sr5-_JM",
      "km": "400.000",
      "year": "1999",
      "meta": "· Publicado hace 9 días",
      "location": "San Fernando - Bs.As. G.B.A. Norte"
    },
    "23": {
      "title": "Toyota Hilux Pick-up Hklux 2.8 Sr5 4x4",
      "price": "19.800",
      "currency": "U$S",
      "link": "https://auto.mercadolibre.com.ar/MLA-1558020868-toyota-hilux-pick-up-hklux-28-sr5-4x4-_JM",
      "km": "191.000",
      "year": "1998",
      "meta": "· Publicado hace 13 días",
      "location": "General San Martín - Bs.As. G.B.A. Norte"
    }
  }
}
```
