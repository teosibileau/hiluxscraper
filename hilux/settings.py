import json
import logging
import os

BOT_NAME = "hilux"

SETTINGS = {
    "db": os.environ.get("TINYDB_PATH", "data/db.json"),
}

logging.debug(json.dumps(SETTINGS))

SPIDER_MODULES = ["hilux.spiders"]
NEWSPIDER_MODULE = "hilux.spiders"

ROBOTSTXT_OBEY = True

DEFAULT_REQUEST_HEADERS = {
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7",  # noqa
    "Accept-Encoding": "gzip, deflate, br",  # noqa
    "Accept-Language": "en-US,en;q=0.9,es-AR;q=0.8,es;q=0.7",  # noqa
    "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/119.0.0.0 Safari/537.36",  # noqa
    "Sec-Ch-Ua": '"Google Chrome";v="89", "Chromium";v="89", ";Not A Brand";v="99"',  # noqa
}

ITEM_PIPELINES = {
    "hilux.pipelines.HiluxPipeline": 300,
}

REQUEST_FINGERPRINTER_IMPLEMENTATION = "2.7"
TWISTED_REACTOR = "twisted.internet.asyncioreactor.AsyncioSelectorReactor"
FEED_EXPORT_ENCODING = "utf-8"
