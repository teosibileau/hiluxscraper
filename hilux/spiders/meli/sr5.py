from hilux.spiders.meli.base import BaseMeliScraper


class SR5Scraper(BaseMeliScraper):
    name = "sr5"

    start_urls = [
        "https://autos.mercadolibre.com.ar/toyota/toyota-hilux-sr5",
    ]

    def parse_hilux(self, response):
        item = super().parse_hilux(response)
        if item["year"] is None:
            return None
        if int(item["year"]) > 2005:
            return None
        return item
