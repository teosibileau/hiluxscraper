from pyquery import PyQuery as pq
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule

from hilux.items import HiluxItem


class BaseMeliScraper(CrawlSpider):
    name = "meli"
    allowed_domains = ["mercadolibre.com.ar"]

    rules = (
        Rule(
            LinkExtractor(
                allow="https://auto.mercadolibre.com.ar/MLA-.*",
            ),
            "parse_hilux",
            follow=True,
        ),
    )

    def parse_hilux(self, response):
        d = pq(response.body)

        self.logger.warning("PROCESSING: %s", response.url)

        title = d(
            ".ui-pdp-container--column-right .ui-pdp-header__title-container h1"
        ).text()

        if "Hilux" not in title:
            return None

        price = d(
            ".ui-pdp-container--column-right"
            " .ui-pdp-price__second-line span.andes-money-amount__fraction"
        ).text()
        currency = d(
            ".ui-pdp-container--column-right"
            " .ui-pdp-price__second-line span.andes-money-amount__currency-symbol"
        ).text()

        header, meta = (
            d(
                ".ui-pdp-container--column-right .ui-pdp-header__subtitle span.ui-pdp-subtitle"
            )
            .text()
            .split("km")
        )
        year, km = header.split("|")
        year = year.strip()

        km = km.strip()
        meta = meta.strip()

        location = d(
            ".ui-seller-info__status-info .ui-seller-info__status-info__subtitle"
        ).text()

        return HiluxItem(
            title=title,
            price=price,
            currency=currency,
            link=response.url,
            km=km,
            year=year,
            meta=meta,
            location=location,
        )
