# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html

import csv

from tinydb import Query, TinyDB

from .settings import SETTINGS


class HiluxPipeline:
    def __init__(self):
        self.db = TinyDB(SETTINGS["db"])
        self.csv_file = open("data/db.csv", "w", newline="")
        self.csv_writer = csv.writer(self.csv_file, delimiter=",")

    def process_item(self, item, spider):
        Hilux = Query()
        truck = self.db.search(Hilux.link == item["link"])

        if not truck:
            doc_id = self.db.insert(item)
            truck = self.db.get(doc_id=doc_id)
            spider.logger.info(f"New truck added: {truck['title']} -> {truck['link']}")
        else:
            truck = truck[0]
            spider.logger.info(f"Truck already exists: {truck['title']}")

        if truck["price"] != item["price"]:
            self.db.update({"price": item["price"]}, doc_ids=[truck.doc_id])
            spider.logger.info(
                f"Price changed from {truck['price']} to {item['price']} for {item['link']}"
            )
        self.csv_writer.writerow(item.values())

    def close_spider(self, spider):
        self.csv_file.close()
