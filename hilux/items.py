import scrapy


class HiluxItem(scrapy.Item):
    title = scrapy.Field()
    price = scrapy.Field()
    currency = scrapy.Field()
    year = scrapy.Field()
    km = scrapy.Field()
    location = scrapy.Field()
    link = scrapy.Field()
    meta = scrapy.Field()
